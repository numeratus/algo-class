

#Your task is to compute the total number of comparisons
#used to sort the given input file by QuickSort.
#As you know, the number of comparisons depends on which
#elements are chosen as pivots, so we'll ask you to
#explore three different pivoting rules.
#You should not count comparisons one-by-one. Rather,
#when there is a recursive call on a subarray of
#length m, you should simply add m-1 to your running
#total of comparisons. (This is because the pivot
#element is compared to each of the other m-1 elements
#in the subarray in this recursive call.)

import numpy as np

file = open("integers.txt")
all_integers = file.readlines()
file.close()

integers = map(int, all_integers)

sample_array = np.array([11, 5, 1, 9, 6, 2, 3, 0, 20, 12])
sample_array = np.random.randint(1,10000000,10000)

def partition_array(array, left, right, pivot_idx):
    #Work with python indexing
    left = left - 1
    right = right - 1
    pivot_idx = pivot_idx - 1
    #print "Lower bound: " + str(array[left])
    #print "Upper bound: " + str(array[right])
    pivot = array[pivot_idx]
    #print "  Pivot: " + str(pivot)
    #Putting pivot at the end, swapping with first
    #print "  Swapping " + str(array[pivot_idx]) + " (pivot) and " + str(array[left]) + " (first)"
    temp = array[pivot_idx]
    array[pivot_idx] = array[left]
    array[left] = temp
    left_idx = left + 1
    for right_idx in np.arange(left + 1, right + 1):
        #print "  Comparing " + str(array[right_idx]) + " to pivot"
        if array[right_idx] < pivot:
            #print "  Swapping " + str(array[left_idx]) + " and " + str(array[right_idx])
            temp = array[right_idx]
            array[right_idx] = array[left_idx]
            array[left_idx]  = temp
            left_idx = left_idx + 1
            #print "    Array: " + str(array)
    #print "  Swapping " + str(array[left]) + " (pivot) and " + str(array[left_idx - 1])
    temp = array[left]
    array[left] = array[left_idx - 1]
    array[left_idx - 1] = temp
    pivot_end_idx = left_idx - 1
    return (pivot_end_idx + 1)

def choose_pivot(array, left, right):
    choices = [array[left-1], array[right-1], array[left + (right - left)/2 - 1]]
    #print "  Comparing " + str(array[left-1]) + ", " + str(array[right-1]) + ", " + str(array[left + (right - left)/2 - 1])
    choices.sort()
    # we need to return the index to the value choice[1]
    if choices[1] == array[left-1]:
        choice_idx = left
    if choices[1] == array[right-1]:
        choice_idx = right
    if choices[1] == array[left + (right - left)/2 - 1]:
        choice_idx = left + (right - left)/2
    #return(choice_idx)
    return(right)
    #return(left)

def quick_sort(array, left, right, comparisons):
    #print "Array: " + str(array)
    length = right - left + 1
    #print " Length: " + str(length)
    if length <= 1: return comparisons
    pivot_idx = choose_pivot(array, left, right)
    pivot_end_idx = partition_array(array, left, right, pivot_idx)
    comparisons = comparisons + length - 1
    comparisons = quick_sort(array, left, pivot_end_idx - 1, comparisons)
    comparisons = quick_sort(array, pivot_end_idx + 1, right, comparisons)
    return comparisons

#sample_array = np.array([11, 5, 1, 9, 6, 2, 3, 0, 20, 12, 68, 15, 32, 28, 34])
#sample_array = np.array([2, 4, 5, 3, 1])
comparisons = quick_sort(sample_array, 1, len(sample_array), 0)
print "Number of comparisons: "+str(comparisons)

comparisons = quick_sort(integers, 1, len(integers), 0)
print "Number of comparisons: "+str(comparisons)

162085
164123
138382
