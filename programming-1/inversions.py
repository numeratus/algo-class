
import numpy as np

file = open("integers.txt")
all_integers = file.readlines()
file.close()

integers = map(int, all_integers)

sample_array = np.array([4, 5, 1, 9, 6, 2, 3, 0, 20, 12])
sample_array = np.random.randint(1,1000,10000)

def calc_inversion_brute(array):
    inversions = 0
    for idx, val in enumerate(array):
        print str(idx) + " of " + str(len(array))
        for idx_down in np.arange(idx, len(array)):
            if array[idx_down] < array[idx]:
                inversions = inversions + 1
    return inversions

        

def calc_inversion_sort(array):
    sorted = np.sort(array, kind='mergesort')
    inversions = 0
    for idx, val in enumerate(array):
        print str(idx) + " of " + str(len(array))
        # find the position of val in sorted
        # every iteration indicates an inversion
        # skip that value in the next iteration
        pos_search = 0
        while (sorted[pos_search] != val):
            inversions = inversions + 1
            pos_search = pos_search + 1
        # when getting out of this loop
        # pos_search is where the value is
        # in the sorted array
        sorted = np.delete(sorted, pos_search)
            
    return inversions

def merge(left, right, inversions):
    result = []
    i ,j = 0, 0
    while i < len(left) and j < len(right):
        if left[i] <= right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1
            inversions += len(left) - i

    result += left[i:]
    result += right[j:]
    return result, inversions

def mergesort(lst, inversions):
    if len(lst) <= 1:
        return lst, inversions
    middle = int( len(lst) / 2 )
    left, inversions = mergesort(lst[:middle], inversions)
    right, inversions = mergesort(lst[middle:], inversions)
    result, inversions = merge(left, right, inversions)
    return result, inversions

#inversions = calc_inversion_brute(sample_array)
#print(inversions)

inversions = calc_inversion_sort(sample_array)
print(inversions)

inversions = calc_inversion_sort(integers)
print(inversions)